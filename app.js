const mongoose = require("mongoose");
const express = require('express');
const db = require("./db");   

const bodyParser = require('body-parser');
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: true});
const emitter = require('./emitter');
const fs = require("fs");

const userRoutes = require('./routes/userRouter');
const adminRoutes = require('./routes/adminRouter');
const homeRoutes = require('./routes/homeRouter');
const roomsRoutes = require('./routes/roomsRouter.js');

app.use(express.json())
app.use('/user', urlencodedParser, userRoutes);
app.use('/admin', adminRoutes);
app.use('/rooms', roomsRoutes);
app.use('/', homeRoutes);

emitter.on('fileEv', function(fileName){
    let readableStream = fs.createReadStream(fileName, "utf8");
    let writeableStream = fs.createWriteStream("newWriteSimple.json");
    readableStream.pipe(writeableStream);
});

app.listen(5000);
  





